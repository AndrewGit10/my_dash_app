import app

class Test:
    '''
    Test class provides unit tests for callback functions in app.py

    Methods:
        test_display_map_input(self):
            Test display_map(limit, color_radio_item) function for right input
        test_display_bar_mean_borough_input(self):
            Test display_bar_mean_borough(limit) function for right input
        test_display_hist_distribution_input(self):
            Test display_hist_distribution(limit) function for right input
    '''

    #def test_import_app(self):
     #   '''
      #  Testing if app is importing properly
       # '''
       # modulename = 'app'
       # if modulename not in sys.modules:     
        #    assert 1==2, "You have not correctly imported app module"
       # else:
        #    assert 1==1, "Error"		

    def test_create_global_deaths(self):
        '''
        Test create_global_deaths(limit) function for right input
        '''
        limits = [1, 15, 20, 25, 30, 35]
        for limit in limits:
            assert 1==1,"Error"
            assert app.create_global_deaths(limit), \
            "Error with testing create_global_deaths(limit) function for right input"

    def test_create_can_graph(self):
        '''
        Test create_can_graph(limit) function for right input
        '''
        limits = [0, 1, 10, 20, 50, 100]
        for limit in limits:
            assert app.create_can_graph(limit), \
                "Error with testing create_can_graph(limit) function for right input"

    def test_create_mort_rate(self):
        '''
        Test create_mort_rate(limit) function for right input
        '''
        limits = [0, 1, 10, 20, 50, 100]
        for limit in limits:
            assert app.create_mort_rate(limit), \
                "Error with testing create_mort_rate(limit) function for right input"


