# Dash App for Lantern Institute Data Science Course - A Covid-19 Analytics Dashboard

In this repository is a simple [Dash](https://dash.plot.ly) application with Python. Contained in here is some simple data analysis used to investigate recent [covid-19 data](https://www.kaggle.com/sudalairajkumar/novel-corona-virus-2019-dataset?select=time_series_covid_19_confirmed_US.csv) released by the World Health Organization (WHO) providing insight to the current status of the global fight agains the corona virus. To visit the WHO covid-19 dashboard click [here](https://covid19.who.int/).

## Project Details
In this application there are several interactive plots dealing with covid-19 data ranging from April 1st, 2021 until May 5th, 2021. This application looks into which countries are suffering the most fatalities, which countries have the highest mortality rates and how the Canadian provinces and territories are managing in comparison to other regions from all over the world. 

The projects has an interactive map of the Earth, colour coding the countries by the population size. If you place your mouse over a country, a three letter acronym with the country code will appear with the population for the country. This is show cases the countries with large populations, such as China and India, that can be significant hot spots for spreading the virus.  

The raw data file for this project is provided as a csv file in the data directory. However there is much more data available for analysis available [here](https://www.kaggle.com/sudalairajkumar/novel-corona-virus-2019-dataset?select=time_series_covid_19_confirmed_US.csv).

## Getting Started With Git
To utilize the interactive dashboard, clone or download the repository by copying `git clone https://gitlab.com/AndrewGit10/my_dash_app.git`. 
Next, create a virtual environment and install the libraries that are listed in the `requirements.txt` file.
To run app with Python simply execute `python3 app.py` and copy and paste `http://127.0.0.1:8050/` in your computers internet browser.  

![](./images/running.png)

This should bring up the something like the image above on your terminal. Once you are on the page, feel free to scroll to the different visual representations of the data and use the sliders to view the current (as of May 5th, 2021) status of different regions around the world.

## What you should see
Map:
![](./images/top.png)

Plots:
![](./images/middle.png)


![](./images/bottom.png)

## Setup with Docker
To run the dashboard with Docker first compile docker by running `sudo docker build -t docker_name .`. Then run `docker run -p 8050:8050 docker_name` to visit the dashboard with Docker, you can do this by copying and pasting http://127.0.0.1:8050/ in your web browser after successfully running the docker command above.


