# -*- coding: utf-8 -*-

# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

import plotly.express as px
import plotly.figure_factory as ff

import pandas as pd
import scipy.stats as sp
import geopandas as gpd

import datetime

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options

def create_map():
    world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
    world = world[(world.pop_est>0) & (world.name!="Antarctica")]
    world = world.set_index('iso_a3')
    mapbox = px.choropleth(world,
                   geojson=world.geometry,
                   locations=world.index,
                   color="pop_est",
                   projection="mercator",
                   width=1200,
                   height=1000)
    mapbox.update(layout_coloraxis_showscale=False)
    mapbox.update_geos(fitbounds="locations", visible=False)
    return mapbox

def create_table(dataframe, max_rows=5):
    '''
    Returns html table based on 'max_rows' random rows of pandas.DataFrame

    Parameters:
        dataframe (pandas.DataFrame) : DataFrame to visualize
        max_rows (int) : count of rows to visualize

    Returns:
        html.Table
    '''

    df = dataframe.sample(n=max_rows)
    return html.Table([
        html.Thead(
            html.Tr([html.Th(col) for col in df.columns])
        ),
        html.Tbody([
            html.Tr([
                html.Td(df.iloc[i][col]) for col in df.columns
            ]) for i in range(min(len(df), max_rows))
        ])
    ])

# Data reading
df_covid = pd.read_csv('./data/covid_19_data.csv',
			  parse_dates=['Last Update'],
                          usecols=['SNo', 'ObservationDate','Province/State','Country/Region',
			 'Last Update','Confirmed','Deaths','Recovered'])

names = {"SNo": "sno",
	 "ObservationDate": "observation_date",
	 "Province/State": "province_state",
	 "Country/Region": "country_region",
	 "Last Update": "last_update",
	 "Confirmed": "confirmed",
	 "Deaths": "deaths",
	 "Recovered": "recovered"}

df_covid.rename(columns=names, inplace=True)

def global_deaths():
    #Comparison of Canadian death rates to global death rates for countries with highest total deaths
    country = df_covid.groupby('country_region')['deaths'].max().reset_index()
    country = country.loc[countr['deaths']>=20000] 

    global_death = px.bar(country,
             x="deaths",
             y ="country_region",
             title="Mortality Rate for Countries With Highest Number of Death From Covid-19",
             labels={ # replaces default labels by column name
                "deaths": "Percentage of Deaths Normalized by Population",  "country_region": "Country"},
	     height = 1000)
    return global_death

BS = "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
external_stylesheets = [BS]
app = dash.Dash(__name__, external_stylesheets=external_stylesheets, title='Covid-19 Analytics')

app.layout = html.Div(children=[
    html.Center(html.H2(children='Covid-19 Analytics Dashboard')),

    html.H4(children='Data about covid-19 from World Health Organization from April 1st, 2021 to May 5th, 2021.'),
    html.A('Click '),
    html.A('here', href='https://www.kaggle.com/sudalairajkumar/novel-corona-virus-2019-dataset?select=time_series_covid_19_confirmed_US.csv'),
    html.A(' for access to the raw data.'),
    html.Br(),

    html.Br(),

    html.Div(
        children=[
            html.Br(),
            html.H4('Interactive visualization of global population by country.'),
            dcc.Graph(id='map', figure=create_map())],
            className='div_border',
    ),


    html.Br(),

    html.Div(
        children=[
            html.Br(),
            html.H4('Countries with the highest mortality rate.'),
            dcc.Graph(id="bar_high_covid"),
            html.H5('Mortality (%)'),
            dcc.Slider(id="limit", 
               min=0, max=25, value=1, step=0.1,
               marks={0: '0',
                      5: '5',
                      10: '10',
                      15: '15',
                      20: '20',
		      25: '25'}),
            html.Br(),
            html.Br()
            ],
            className='left',
    ),

    html.Br(),

    html.Div(
        children=[
            html.Br(),
            html.H4('Mortality rates for Canadian provinces and territories.'),
        dcc.Graph(
            id='percent_death'
            ),
            html.H5('Mortality (%)'),
            dcc.Slider(id="limitCan", 
               min=0, max=5, value=1, step=1,
               marks={0: '0',
                      1: '1',
                      2: '2',
                      3: '3',
                      4: '4',
                      5: '5'}),
            ],
            className='right',
    ),
  
    html.Div(
        children=[
            html.Br(),
            html.H4('Mortality rates for Canadian provinces and territories.'),
        dcc.Graph(
            id='global_death'),
        html.H5('Number of Deaths'),
            dcc.Slider(id="limitGlo",
               min=10000, max=150000, value=1, step=2500,
               marks={10000: '10000',
                      30000: '30000',
                      60000: '60000',
                      90000: '90000',
                      120000: '120000',
                      150000: '150000'}),
            html.Br(),
            html.Br()
            ],
            className='bottom',
    ),


])

def create_mort_rate(limit):
    '''
    Display the deaths for each country from covid-19 from April 1st, 2021 to May 5th, 2021.

    Parameters:
        limitGlo (int / float) : Central value of the plot display to display

    Returns:
        px.bar object to display
    '''
    assert isinstance(limit, int) or isinstance(
        limit, float), "Limit must be integer or float value"
    assert limit >= 0, "Limit must be positive value"    
    mortality = df_covid.groupby('country_region')['deaths'].max().reset_index()
    confirmed = df_covid.groupby('country_region')['confirmed'].max().reset_index()

    rate = pd.merge(left=mortality,right=confirmed, left_on='country_region', right_on='country_region')

    rate = rate.loc[rate['confirmed']>0]
    rate['mortality_rate'] = 100*rate['deaths']/rate['confirmed']
    rate = rate.loc[(rate['mortality_rate']<limit+0.2)&(rate['mortality_rate']>limit-0.2)]

    mort_rate = px.bar(rate,
             x="mortality_rate",
             y ="country_region",
             title="Countries With The Highest Mortality Rate From Covid-19",
            labels={ # replaces default labels by column name
                "mortality_rate": "Mortality Rate",  "country_region": "Country"
            } )
    return mort_rate

def create_can_graph(limit):
    '''
    Display the deaths for each country from covid-19 from April 1st, 2021 to May 5th, 2021.

    Parameters:
        limitGlo (int / float) : Central value of the plot display to display

    Returns:
        px.bar object to display
    '''
    assert isinstance(limit, int) or isinstance(
        limit, float), "Limit must be integer or float value"
    assert limit >= 0, "Limit must be positive value"

    # Data cleaning
    # Looking solely at Canadian data columns and ensuring to remove data with na values
    can = df_covid.loc[df_covid['country_region'] == 'Canada']
    can.dropna(inplace=True)

    #We also only care about the dates, so lets get rid of the time and focus only on the recent time 
    #where we have useful data 
    can['last_update'] = pd.to_datetime(can['last_update'],utc=True)
    can['last_update'] = can['last_update'].dt.date
    date_before = datetime.date(2021, 4, 2)
    can = can.loc[can['last_update'] > date_before]
    
    #First lets look at the number of deaths for each province
    #for this we also want to normalize the results by each provinces population
    #The province population data came from Ref:(https://www150.statcan.gc.ca/t1/tbl1/en/tv.action?pid=1710000901)
    #Additionally there are a few locations (ie cruise ships etc) which are not provinces
    #lets get rid of these as they contribute an insignificant amount of data. 
    deaths_c = can.groupby('province_state')['deaths'].max().reset_index()
    confirmed_c = can.groupby('province_state')['confirmed'].max().reset_index()
    deaths_c = deaths_c.loc[(deaths_c['province_state']!="Repatriated Travellers")
                            &(deaths_c['province_state']!="Grand Princess")
                            &(deaths_c['province_state']!="Diamond Princess cruise ship")]
    confirmed_c = confirmed_c.loc[(confirmed_c['province_state']!="Repatriated Travellers")
                                 &(confirmed_c['province_state']!="Grand Princess")
                                 &(confirmed_c['province_state']!="Diamond Princess cruise ship")]
    deaths_c['deaths'] = 100*deaths_c['deaths']/confirmed_c['confirmed']
    deaths_c = deaths_c.loc[(deaths_c['deaths']<limit+1)&(deaths_c['deaths']>limit-1)]
    fig = px.bar(deaths_c,
                 x="deaths",
                 y ="province_state",
                 title="Canadian Death Rate For Each Province From Covid-19",
                 labels={
                         "deaths": "Percentage of Deaths Normalized by Population",
                         "province_state": "Province"}
                )
    return fig

def create_global_deaths(limit):
    '''
    Display the deaths for each country from covid-19 from April 1st, 2021 to May 5th, 2021.

    Parameters:
        limitGlo (int / float) : Central value of the plot display to display

    Returns:
        px.bar object to display
    '''

    assert isinstance(limit, int) or isinstance(limit, float), "Limit must be integer or float value"
    assert limit >= 0, "Limit must be positive value"

    country = df_covid.groupby('country_region')['deaths'].max().reset_index()
    country = country.loc[country['deaths']>5000]
    if(limit < 10000):
        country = country.loc[(country['deaths']<=limit+5000)&(country['deaths']>=0)]
    else:
        country = country.loc[(country['deaths']<=limit+5000)&(country['deaths']>=limit-5000)]

    global_death_fig = px.bar(country,
             x="deaths",
             y ="country_region",
             title="Mortality Rate for Countries With Highest Number of Death From Covid-19",
             labels={
                "deaths": "Percentage of Deaths Normalized by Population",  "country_region": "Country"},
             height = 1000)
    return global_death_fig

app.callback(
        Output("bar_high_covid", "figure"),
        [Input("limit", "value")])(create_mort_rate)

app.callback(
        Output("percent_death", "figure"),
        [Input("limitCan", "value")])(create_can_graph)

# Callback for bar display
app.callback(
    Output("global_death", "figure"),
    [Input("limitGlo", "value")]
)(create_global_deaths)



if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0', port=8050)

